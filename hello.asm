global      _start

section     .text

_start:
    
    jmp     _write              ; write std_out
    jmp     _exit               ; exit program

_write:
    
    mov     rax, 1              ; syscall -> write
    mov     rdi, 1              ; fd -> 1 == stdout
    mov     rsi, msg            ; *buf -> &msg
    mov     rdx, 9              ; nbytes
    syscall

_exit:
    
    mov     rax, 60             ; syscall -> exit
    mov     rdi, 0              ; code -> 0
    syscall

section     .data

msg:
    db      "hello", 0xA        ; string + newline

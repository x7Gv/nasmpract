section .data

msg:
    db	"str_len ght.", 0xA     ; str + \n

section .text

global _start

_start:

    mov     rbx, msg            ; &msg -> rbx
    mov     rax, rbx            ; rbx -> rax (length of the string)

_nextchar:

    cmp     byte [rax], 0       ; msg[rax] == 0
    jz      _finished           ; true (pointer is at the end of string)
    inc     rax                 ; rax++
    jmp     _nextchar           ; false (loop back)

_finished:

    sub     rax, rbx
   
    mov     rdx, rax
    mov     rsi, msg

    mov     rax, 1
    mov     rdi, 1
    syscall

    jmp     _exit

_exit:
    mov     rax, 60
    mov     rdi, 0
    syscall

# nasmpract

> NASM -ic personal first steps archive

This repository is a personal archive meant for files that are touched during the author's first steps on practising Netwide Assembler (NASM) programming.

With that being stated, this should not be looked as in any kind of reference to the correct way of using NASM and it's more than likely for this to expect causing infuriating facepalm-ish moments among experienced NASM hackers. You have been warned.
##
# nasmpract
#
# @file
# @version 0.1

PROG = hello

LD = ld
NASM = nasm

# LD_FLAGS = -melf_i386
NASM_FLAGS = -felf64

OBJS = $(PROG).o

default: $(PROG)

$(PROG): $(OBJS)
	$(LD) $(OBJS) -o $(PROG)

$(PROG).o: $(PROG).asm
	$(NASM) $(NASM_FLAGS) $(PROG).asm

clean:
	rm -rf *.o *~

cleanall:
	rm -rf *.o $(PROG) *~

# end
